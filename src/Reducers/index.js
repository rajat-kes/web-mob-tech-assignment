import storage from 'redux-persist/lib/storage'
import { persistCombineReducers } from 'redux-persist'

import Movies from '../Container/Movies/reducer';
import MovieDetsils from '../Container/MovieDetsils/reducer';
const reducers = {
    'movie': Movies,
    'movieDetsils': MovieDetsils,
}

export const persistConfig = {
    key: 'ASSIGNMENT.0.0.1',
    storage,
    blacklist: [
        'movie',
        'movieDetsils'
    ]
}

const appReducer = persistCombineReducers(persistConfig, reducers)

const rootReducer = (state, action) => {
    return appReducer(state, action)
};

export default rootReducer;