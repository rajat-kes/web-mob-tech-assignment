
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import PageNotFound from '../Container/Pagenotfound';
// import withHeaderWrapper from '../Component/Comman/Header/withHeaderWrapper';
// import Home from '../Container/Home';
import Movies from '../Container/Movies';
import MovieDetails from '../Container/MovieDetsils';

const routes = (props) => {
    return (
        <Router>
            <Switch>
                <Route
                    exact
                    path="/"
                    component={Movies} />
                <Route
                    exact
                    path="/:id/movie"
                    component={MovieDetails} />
                <Route
                    path=""
                    component={PageNotFound} />
            </Switch>
        </Router>
    );
}

export default routes;