import React, { Component } from 'react'

import { Link } from 'react-router-dom';
import './style.css';
export class Home extends Component {
    render() {
        return (
            <header className="masthead">
                <div className="container h-100">
                    <div className="h-100 align-items-center justify-content-center text-center">
                        <div className="align-self-end">
                            <h1 className="text-uppercase text-white font-weight-bold">
                                {`Welcome to this project`}
                            </h1>
                            <hr className="divider my-4" />
                        </div>
                        <div className="align-self-baseline">
                            <p className="text-white-75 font-weight-light mb-5">
                                {`This Project use React js designed by Rajat Keserwani`}
                            </p>
                            <Link className="btn btn-primary btn-xl js-scroll-trigger" to="/movies">{`Find Out Project`}</Link>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Home
