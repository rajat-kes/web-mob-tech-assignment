import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import StarIcon from '@material-ui/icons/Star';
import { movieImageBaseURL } from '../../../util/staticValue';
import './style.css';

const MovieCard = (props) => {
    const { movieDetails } = props
    const handleChangeURL = (url) => {
        const { changeURL } = props
        changeURL(`/${url}/movie`);
    }
    return (
        < Card className='movie-card' onClick={() => handleChangeURL(movieDetails.id)}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="Poster not available"
                    height="300"
                    image={movieImageBaseURL + movieDetails.poster_path}
                    title="Contemplative Reptile"
                />
                <CardContent  >
                    <div className='movie-title'>
                        <Typography variant="body2" component='span'>
                            {movieDetails.original_title}
                        </Typography>

                        <Typography variant="body2" component='span'>
                            {movieDetails.vote_average}
                            <StarIcon />
                        </Typography>
                    </div>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {movieDetails.overview}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card >
    )
}

export default (MovieCard);
