import React, { Component } from 'react'

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as action from './Actions';

import { withRouter } from "react-router";

import SelectMovieType from './SelectMoviesType'
import MovieCard from './MovieCard';
import './style.css';

const movieTypes = ['Now Playing', 'Latest', 'Upcoming']
class Movies extends Component {
    state = {
        movieType: 'Now Playing',
        // movieType: 'Latest',
        pageNO: 1,
    }

    changeHandlerMovieType = (value) => {
        if (value) {
            this.setState({
                movieType: value,
            }, () => {
                this.getMovieList();
            })
        }
    }

    getMovieList = () => {
        const { getMovieList } = this.props;
        const { movieType, pageNO } = this.state;
        getMovieList({ movieType, pageNO });
    }

    componentDidMount = () => {
        this.getMovieList();
    }

    changeURL = (url) => {
        this.props.history.push(url);
    }

    render() {
        const { movieType } = this.state;
        const { moviesList } = this.props
        const { changeHandlerMovieType, changeURL } = this;
        return (
            <div className="inner-container">
                <div className="block--right">
                    <SelectMovieType
                        list={movieTypes}
                        value={movieType}
                        changeHandler={changeHandlerMovieType} />
                </div>
                <div className='movies-card'>
                    {moviesList.map(movie => {
                        return (
                            <MovieCard
                                key={Math.random()}
                                movieDetails={movie}
                                changeURL={changeURL} />
                        )
                    })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        moviesList: state.movie.movieList,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...action
    }, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Movies))
