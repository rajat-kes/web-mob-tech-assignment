import * as types from './ActionTypes';

const initialState = {
    movieList: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_MOVIE_LIST:
            return {
                ...state,
                movieList: action.payload,
            }
        case types.CLEAR_MOVIE_LIST:
            return {
                ...state,
                movieList: initialState.movieList,
            }
        default:
            return state
    }
}
