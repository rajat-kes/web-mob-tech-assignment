import * as types from './ActionTypes';
import axios from 'axios';

import { movieApiKey } from '../../util/staticValue';

const clearMovieLsit = () => {
    return (dispatch, getState) => {
        return dispatch({ type: types.CLEAR_MOVIE_LIST, payload: [] })
    }
}

const getMovieUrl = (data) => {
    let URL = 'https://api.themoviedb.org/3/movie/'
    switch (data.movieType) {
        case 'Latest':
            URL = URL + 'latest'
            break;
        case 'Now Playing':
            URL = URL + 'now_playing'
            break;
        case 'Upcoming':
        default:
            URL = URL + 'upcoming'
    }
    return (URL + `?api_key=${movieApiKey}&language=en-US&page=${data.pageNO || 1}`)
}
export const getMovieList = (data) => {
    return (dispatch, getState) => {
        dispatch(clearMovieLsit())
        const url = getMovieUrl(data)
        axios.get(url)
            .then((res) => {
                let newMovieList = res.data.results || []
                if(data.movieType === 'Latest'){
                    newMovieList = [res.data]
                }
                return dispatch({ type: types.SET_MOVIE_LIST, payload: newMovieList });
            })
            .catch((error) => {
                console.log(error);
            });
    }
}