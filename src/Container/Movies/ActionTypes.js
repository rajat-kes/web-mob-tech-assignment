const pre = 'SPOTIFY_';

export const SET_MOVIE_LIST = pre + 'SET_MOVIE_LIST'

export const CLEAR_MOVIE_LIST = pre + 'CLEAR_MOVIE_LIST'