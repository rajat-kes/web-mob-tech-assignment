import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { movieImageBaseURL } from '../../../util/staticValue';

const useStyles = makeStyles({
    root: {
        maxWidth: '100em',
        margin: '1em',
    },
    media: {
        height: '25em',
    },
});

const MovieDetailsCard = (props) => {
    const classes = useStyles();
    const { poster_path, title, overview } = props.data
    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={movieImageBaseURL + poster_path}
                    title="{Poster not available}"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {overview}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
            </CardActions>
        </Card>
    );
}

export default MovieDetailsCard;