import React, { Component } from 'react'

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as action from './Actions';

import { withRouter } from "react-router";

import Button from '@material-ui/core/Button';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import DisplayMovieCard from './Card';
import './style.css';
export class MovieDetsils extends Component {

    changeURL = (url) => {
        this.props.history.push(url);
    }

    callgetMovieDetails = (id) => {
        const { getMovieDetails } = this.props;
        getMovieDetails({ id });
    }

    componentDidMount = () => {
        if (this.props.match.params.id) {
            this.callgetMovieDetails(parseInt(this.props.match.params.id));
        }
    }

    render() {
        const { changeURL } = this
        const { movieDetail } = this.props
        return (
            <div>
                <div className="block--right">
                    <Button
                        variant="contained"
                        color="primary"
                        endIcon={<ArrowBackIcon />}
                        onClick={() => changeURL('/')}
                    >
                        {'Go Back'}
                    </Button>
                </div>
                <DisplayMovieCard data={movieDetail} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        movieDetail: state.movieDetsils.movieDetail,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...action
    }, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieDetsils))
