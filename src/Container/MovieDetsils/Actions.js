import * as types from './ActionTypes';
import axios from 'axios';

import { movieApiKey } from '../../util/staticValue';

const clearMovieDetails = () => {
    return (dispatch, getState) => {
        return dispatch({ type: types.CLEAR_MOVIE_DETAILS, payload: {} })
    }
}

const getMovieUrl = (data) => {
    let URL = 'https://api.themoviedb.org/3/movie/'
    return (URL + `${data.id}?api_key=${movieApiKey}&language=en-US`)
}
export const getMovieDetails = (data) => {
    if (data.id) {
        return (dispatch, getState) => {
            dispatch(clearMovieDetails())
            const url = getMovieUrl(data)
            axios.get(url)
                .then((res) => {
                    return dispatch({ type: types.SET_MOVIE_DETAILS, payload: res.data });
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }
}