const pre = 'SPOTIFY_';

export const SET_MOVIE_DETAILS = pre + 'SET_MOVIE_DETAILS'

export const CLEAR_MOVIE_DETAILS = pre + 'CLEAR_MOVIE_DETAILS'