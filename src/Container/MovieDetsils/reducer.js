import * as types from './ActionTypes';

const initialState = {
    movieDetail: {},
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_MOVIE_DETAILS:
            return {
                ...state,
                movieDetail: action.payload,
            }
        case types.CLEAR_MOVIE_DETAILS:
            return {
                ...state,
                movieDetail: initialState.movieDetail,
            }
        default:
            return state
    }
}
